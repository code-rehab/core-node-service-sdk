export declare const getAccountID: () => Promise<string>;
export declare const setAccountID: (id: string) => string;
export declare const getRegion: () => Promise<string>;
