declare type ClassRecord<T> = Record<
  keyof T,
  {
    new (...args: any[]): T[keyof T];
  }
>;
interface ServiceModules<Resources, Providers, Services> {
  resources: ClassRecord<Resources>;
  providers: ClassRecord<Providers>;
  services: ClassRecord<Services>;
}
interface Resolver {
  invoke(event: any, context: any): any;
}
interface ServiceOptions {
  xray: boolean;
}
interface ServiceConfig extends ServiceOptions {}
declare type ServiceResolver<Resources> = (resources: Resources) => Resolver;
export declare class Service<Resources, Providers, Services> {
  private _serviceResolver;
  private _moduleClasses;
  private _modules;
  resources: Resources;
  services: Services;
  providers: Providers;
  resolver: Resolver;
  config: ServiceConfig;
  constructor(
    _serviceResolver: ServiceResolver<Resources>,
    _moduleClasses: ServiceModules<Resources, Providers, Services>,
    options?: Partial<ServiceOptions>
  );
  invoke: (_event: any, _context: any) => Promise<any>;
  /**
   * Registers a Resource class ready to be lazy loaded
   * @param key name of the Service
   * @param ClassName Class of the Service
   */
  private _registerService;
  /**
   * Registers a Resource class ready to be lazy loaded
   * @param key name of the Provider
   * @param ClassName Class of the Provider
   */
  private _registerProvider;
  /**
   * Registers a Resource class ready to be lazy loaded
   * @param key name of the resource
   * @param ClassName Class of the Resource
   */
  private _registerResource;
}
export {};
