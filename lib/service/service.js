"use strict";
var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function(resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (!((t = _.trys), (t = t.length > 0 && t[t.length - 1])) && (op[0] === 6 || op[0] === 2)) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var Service = /** @class */ (function() {
  function Service(_serviceResolver, _moduleClasses, options) {
    var _this = this;
    this._serviceResolver = _serviceResolver;
    this._moduleClasses = _moduleClasses;
    this._modules = {
      provider: {},
      service: {},
      resource: {}
    };
    this.resources = {};
    this.services = {};
    this.providers = {};
    this.config = {
      xray: true
    };
    this.invoke = function(_event, _context) {
      return __awaiter(_this, void 0, void 0, function() {
        return __generator(this, function(_a) {
          switch (_a.label) {
            case 0:
              return [4 /*yield*/, this.resolver.invoke(_event, _context)];
            case 1:
              return [2 /*return*/, _a.sent()];
          }
        });
      });
    };
    /**
     * Registers a Resource class ready to be lazy loaded
     * @param key name of the Service
     * @param ClassName Class of the Service
     */
    this._registerService = function(key, ClassName) {
      Object.defineProperty(_this.services, key, {
        get: function() {
          if (!_this._modules.service[key]) {
            _this._modules.service[key] = new ClassName();
          }
          return _this._modules.service[key];
        }
      });
    };
    /**
     * Registers a Resource class ready to be lazy loaded
     * @param key name of the Provider
     * @param ClassName Class of the Provider
     */
    this._registerProvider = function(key, ClassName) {
      Object.defineProperty(_this.providers, key, {
        get: function() {
          if (!_this._modules.provider[key]) {
            _this._modules.provider[key] = new ClassName();
          }
          return _this._modules.provider[key];
        }
      });
    };
    /**
     * Registers a Resource class ready to be lazy loaded
     * @param key name of the resource
     * @param ClassName Class of the Resource
     */
    this._registerResource = function(key, ClassName) {
      Object.defineProperty(_this.resources, key, {
        get: function() {
          if (!_this._modules.resource[key]) {
            _this._modules.resource[key] = new ClassName(_this);
          }
          return _this._modules.resource[key];
        }
      });
    };
    if (options) {
      this.config = __assign(__assign({}, this.config), options);
    }
    Object.keys(this._moduleClasses.services).forEach(function(key) {
      _this._registerService(key, _this._moduleClasses.services[key]);
    });
    Object.keys(this._moduleClasses.providers).forEach(function(key) {
      _this._registerProvider(key, _this._moduleClasses.providers[key]);
    });
    Object.keys(this._moduleClasses.resources).forEach(function(key) {
      _this._registerResource(key, _this._moduleClasses.resources[key]);
    });
    this.resolver = this._serviceResolver(this.resources);
  }
  return Service;
})();
exports.Service = Service;
