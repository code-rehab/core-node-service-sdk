export declare class ServiceRouter<Routes> {
  routes: Routes;
  constructor(routes: Routes);
  get: (name: keyof Routes) => any;
  private _routeUnhandledError;
}
