import { RouteResolverEvent } from "./route-resolver";
export declare type ResourceResolver<EventType = RouteResolverEvent, ResponseType = any> = (
  event: EventType
) => Promise<ResponseType>;
export declare class RouteResource {}
