import { ServiceRouter } from "./router";
import { UserProfileRecord } from "../models/user-profile";
export interface RouteResolverEvent<Arguments = any, Source = any, identity = any> {
  route?: string;
  field?: any;
  source?: Source;
  profile?: UserProfileRecord;
  arguments: Arguments;
  identity: identity;
}
export declare class RouteResolver<Routes extends Record<string, (event: RouteResolverEvent) => any>> {
  routes: Routes;
  xray: boolean;
  router: ServiceRouter<Routes>;
  constructor(routes: Routes);
  invoke: (
    event: RouteResolverEvent<any, any, any> | RouteResolverEvent<any, any, any>[],
    _context: any
  ) => Promise<any>;
  resolveEvent: (event: RouteResolverEvent<any, any, any>) => Promise<any>;
  resolveBatchEvent: (events: RouteResolverEvent<any, any, any>[]) => Promise<any[]>;
}
