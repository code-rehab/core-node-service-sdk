import { Model, BaseModel, RecordData } from "./base-model";
declare type Permissions = {
  allow: string[];
  deny: string[];
};
export interface UserProfileRecord extends RecordData {
  id: string;
  role: string;
  profile: string;
  groups: string[];
  permissions: Permissions;
}
export interface UserProfile extends Model<UserProfileRecord> {
  can(permission: string | string[]): boolean;
}
export declare class UserProfileModel extends BaseModel<UserProfileRecord> implements UserProfile {
  id: string;
  role: string;
  profile: string;
  groups: any[];
  created_at: number;
  updated_at: number;
  permissions: Permissions;
  can: (permission: string | string[]) => boolean;
  private _grants;
  private _checkPermissions;
}
export {};
