"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : ((__.prototype = b.prototype), new __());
    };
  })();
var __decorate =
  (this && this.__decorate) ||
  function(decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? (desc = Object.getOwnPropertyDescriptor(target, key)) : desc,
      d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
      r = Reflect.decorate(decorators, target, key, desc);
    else
      for (var i = decorators.length - 1; i >= 0; i--)
        if ((d = decorators[i])) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
var __importDefault =
  (this && this.__importDefault) ||
  function(mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
var base_model_1 = require("./base-model");
var v4_1 = __importDefault(require("uuid/v4"));
var model_data_1 = require("../decorators/model-data");
var UserProfileModel = /** @class */ (function(_super) {
  __extends(UserProfileModel, _super);
  function UserProfileModel() {
    var _this = (_super !== null && _super.apply(this, arguments)) || this;
    _this.id = v4_1.default();
    _this.role = "";
    _this.profile = "";
    _this.groups = [];
    _this.created_at = 0;
    _this.updated_at = 0;
    _this.permissions = {
      allow: [],
      deny: []
    };
    _this.can = function(permission) {
      var result = { allowed: false, denied: false };
      if (typeof permission === "string") {
        permission = [permission];
      }
      result = permission.reduce(function(result, p) {
        return {
          allowed: result.allowed && _this._checkPermissions(p, _this.permissions.allow),
          denied: result.denied || _this._checkPermissions(p, _this.permissions.deny)
        };
      }, result);
      return result.allowed === true && result.denied === false;
    };
    _this._grants = function(requestedPermission, profilePermission) {
      var isPermissionGroup = profilePermission.indexOf(".*") !== -1;
      var groupName = profilePermission.replace(".*", "");
      return {
        // for example: *
        fullAccess: profilePermission === "*",
        // for example: component.* or component.resource.*
        componentAccess: isPermissionGroup && requestedPermission.startsWith(groupName),
        // for example component.resource.permission
        permissionAccess: requestedPermission === profilePermission
      };
    };
    _this._checkPermissions = function(permission, toValidate) {
      var match = false;
      for (var profilePermission in toValidate) {
        var granted = _this._grants(permission, profilePermission);
        if (granted.fullAccess || granted.componentAccess || granted.permissionAccess) {
          match = true;
          break;
        }
      }
      return match === true;
    };
    return _this;
  }
  __decorate([model_data_1.data], UserProfileModel.prototype, "id", void 0);
  __decorate([model_data_1.data], UserProfileModel.prototype, "role", void 0);
  __decorate([model_data_1.data], UserProfileModel.prototype, "profile", void 0);
  __decorate([model_data_1.data], UserProfileModel.prototype, "groups", void 0);
  __decorate([model_data_1.data], UserProfileModel.prototype, "created_at", void 0);
  __decorate([model_data_1.data], UserProfileModel.prototype, "updated_at", void 0);
  __decorate([model_data_1.data], UserProfileModel.prototype, "permissions", void 0);
  return UserProfileModel;
})(base_model_1.BaseModel);
exports.UserProfileModel = UserProfileModel;
