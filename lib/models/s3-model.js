"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : ((__.prototype = b.prototype), new __());
    };
  })();
var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function(resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (!((t = _.trys), (t = t.length > 0 && t[t.length - 1])) && (op[0] === 6 || op[0] === 2)) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var base_model_1 = require("./base-model");
var helpers_1 = require("../helpers");
var aws_sdk_1 = require("aws-sdk");
var s3_1 = require("../helpers/s3");
var S3BaseModel = /** @class */ (function(_super) {
  __extends(S3BaseModel, _super);
  function S3BaseModel(data) {
    var _this = _super.call(this, data) || this;
    _this.s3 = new aws_sdk_1.S3();
    _this.S3Meta = {};
    _this.keyTemplate = ":id";
    _this.metadata = {};
    _this.S3BucketSuffix = function() {
      return __awaiter(_this, void 0, void 0, function() {
        var _a, _b;
        return __generator(this, function(_c) {
          switch (_c.label) {
            case 0:
              _a = "-";
              return [4 /*yield*/, helpers_1.getAccountID()];
            case 1:
              _b = _a + _c.sent() + "-";
              return [4 /*yield*/, helpers_1.getRegion()];
            case 2:
              return [2 /*return*/, _b + _c.sent()];
          }
        });
      });
    };
    _this.S3Location = function() {
      return __awaiter(_this, void 0, void 0, function() {
        var _a;
        return __generator(this, function(_b) {
          switch (_b.label) {
            case 0:
              _a = "" + this.S3Bucket;
              return [4 /*yield*/, this.S3BucketSuffix()];
            case 1:
              return [2 /*return*/, _a + _b.sent() + "/" + this.S3Key];
          }
        });
      });
    };
    _this.fetch = function() {
      return __awaiter(_this, void 0, void 0, function() {
        var _a;
        return __generator(this, function(_b) {
          switch (_b.label) {
            case 0:
              _a = this;
              return [4 /*yield*/, this._fetchRecord()];
            case 1:
              _a.record = _b.sent();
              return [2 /*return*/];
          }
        });
      });
    };
    _this.save = function(requestParams) {
      return __awaiter(_this, void 0, void 0, function() {
        var dataToStore, params, _a, _b, existingData, key, response, err_1;
        return __generator(this, function(_c) {
          switch (_c.label) {
            case 0:
              _c.trys.push([0, 6, , 7]);
              dataToStore = this.serialize();
              _a = {};
              _b = this.S3Bucket;
              return [4 /*yield*/, this.S3BucketSuffix()];
            case 1:
              params = __assign.apply(void 0, [
                __assign.apply(void 0, [
                  ((_a.Bucket = _b + _c.sent()), (_a.Key = this.S3Key), _a),
                  requestParams || {}
                ]),
                { Metadata: __assign({}, this.metadata) }
              ]);
              return [
                4 /*yield*/,
                this._fetchRecord({ Bucket: params.Bucket, Key: params.Key }).catch(function(_err) {})
              ];
            case 2:
              existingData = _c.sent();
              if (existingData) {
                params.Body = JSON.stringify(__assign(__assign({}, existingData), dataToStore));
              } else {
                params.Body = JSON.stringify(__assign({}, dataToStore));
              }
              key = s3_1.formatRecordKey(this.keyTemplate, this) + this.S3Filename;
              if (!(this.key !== key)) return [3 /*break*/, 4];
              return [4 /*yield*/, this.delete()];
            case 3:
              _c.sent();
              this.key = params.Key = key;
              _c.label = 4;
            case 4:
              return [4 /*yield*/, this.s3.putObject(params).promise()];
            case 5:
              response = _c.sent();
              this.S3Meta.ETag = response.ETag;
              this.S3Meta.VersionId = response.VersionId;
              return [3 /*break*/, 7];
            case 6:
              err_1 = _c.sent();
              throw new Error("Unable to store data on S3 \n" + err_1);
            case 7:
              return [2 /*return*/];
          }
        });
      });
    };
    _this.fetchMeta = function(requestParams) {
      return __awaiter(_this, void 0, void 0, function() {
        var params, _a, _b, S3Response, err_2;
        return __generator(this, function(_c) {
          switch (_c.label) {
            case 0:
              _c.trys.push([0, 3, , 4]);
              _a = {};
              _b = this.S3Bucket;
              return [4 /*yield*/, this.S3BucketSuffix()];
            case 1:
              params = __assign.apply(void 0, [
                ((_a.Bucket = _b + _c.sent()), (_a.Key = this.S3Key), _a),
                requestParams || {}
              ]);
              return [4 /*yield*/, this.s3.headObject(params).promise()];
            case 2:
              S3Response = _c.sent();
              this.S3Meta.ETag = S3Response.ETag;
              this.S3Meta.VersionId = S3Response.VersionId;
              this.key = params.Key;
              return [3 /*break*/, 4];
            case 3:
              err_2 = _c.sent();
              throw new Error("Unable to fetch Metadata from S3 \n" + err_2);
            case 4:
              return [2 /*return*/];
          }
        });
      });
    };
    _this.delete = function(requestParams) {
      return __awaiter(_this, void 0, void 0, function() {
        var params, _a, _b, response, err_3;
        return __generator(this, function(_c) {
          switch (_c.label) {
            case 0:
              _c.trys.push([0, 3, , 4]);
              _a = {};
              _b = this.S3Bucket;
              return [4 /*yield*/, this.S3BucketSuffix()];
            case 1:
              params = __assign.apply(void 0, [
                ((_a.Bucket = _b + _c.sent()), (_a.Key = this.S3Key), _a),
                requestParams || {}
              ]);
              return [4 /*yield*/, this.s3.deleteObject({ Bucket: params.Bucket, Key: params.Key }).promise()];
            case 2:
              response = _c.sent();
              this.S3Meta.VersionId = response.VersionId;
              return [3 /*break*/, 4];
            case 3:
              err_3 = _c.sent();
              throw new Error("Unable to remove object from S3 \n" + err_3);
            case 4:
              return [2 /*return*/];
          }
        });
      });
    };
    _this._fetchRecord = function(requestParams) {
      return __awaiter(_this, void 0, void 0, function() {
        var params, _a, _b, S3Response, response, err_4;
        return __generator(this, function(_c) {
          switch (_c.label) {
            case 0:
              _c.trys.push([0, 3, , 4]);
              _a = {};
              _b = this.S3Bucket;
              return [4 /*yield*/, this.S3BucketSuffix()];
            case 1:
              params = __assign.apply(void 0, [
                ((_a.Bucket = _b + _c.sent()), (_a.Key = this.S3Key), _a),
                requestParams || {}
              ]);
              return [4 /*yield*/, this.s3.getObject(params).promise()];
            case 2:
              S3Response = _c.sent();
              response = JSON.parse(S3Response.Body && S3Response.Body.toString("utf-8"));
              this.S3Meta.ETag = S3Response.ETag;
              this.S3Meta.VersionId = S3Response.VersionId;
              this.key = params.Key;
              return [2 /*return*/, __assign(__assign({}, this.record), response)];
            case 3:
              err_4 = _c.sent();
              throw new Error("Unable to fetch data from S3 \n" + err_4);
            case 4:
              return [2 /*return*/];
          }
        });
      });
    };
    return _this;
  }
  Object.defineProperty(S3BaseModel.prototype, "S3Bucket", {
    get: function() {
      return "undefined";
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(S3BaseModel.prototype, "S3Key", {
    get: function() {
      return this.key || s3_1.formatRecordKey(this.keyTemplate, this) + this.S3Filename;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(S3BaseModel.prototype, "S3Filename", {
    get: function() {
      return "/data.json";
    },
    enumerable: true,
    configurable: true
  });
  return S3BaseModel;
})(base_model_1.BaseModel);
exports.S3BaseModel = S3BaseModel;
