import { BaseModel, RecordData, Model } from "../base-model";
declare type TestModelRecord = RecordData<{
  id: string;
  name: string;
  description: string;
  status: string;
  someObj: any;
}>;
declare type Test = Model<TestModelRecord & {}>;
export declare class TestModel extends BaseModel<TestModelRecord> implements Test {
  id: string;
  name: string;
  description: string;
  status: string | undefined;
  created_at: number | undefined;
  updated_at: number | undefined;
  someObj: any;
}
export {};
