"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : ((__.prototype = b.prototype), new __());
    };
  })();
var __decorate =
  (this && this.__decorate) ||
  function(decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? (desc = Object.getOwnPropertyDescriptor(target, key)) : desc,
      d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
      r = Reflect.decorate(decorators, target, key, desc);
    else
      for (var i = decorators.length - 1; i >= 0; i--)
        if ((d = decorators[i])) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function(resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (!((t = _.trys), (t = t.length > 0 && t[t.length - 1])) && (op[0] === 6 || op[0] === 2)) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var base_model_1 = require("../base-model");
var model_data_1 = require("../../decorators/model-data");
var TestModel = /** @class */ (function(_super) {
  __extends(TestModel, _super);
  function TestModel() {
    var _this = (_super !== null && _super.apply(this, arguments)) || this;
    _this.id = "id-generator";
    _this.name = "Unknown";
    _this.description = "Unknown";
    _this.created_at = 123;
    _this.updated_at = undefined;
    _this.someObj = { id: "something" };
    return _this;
  }
  __decorate([model_data_1.data], TestModel.prototype, "id", void 0);
  __decorate([model_data_1.data], TestModel.prototype, "name", void 0);
  __decorate([model_data_1.data], TestModel.prototype, "description", void 0);
  __decorate([model_data_1.data], TestModel.prototype, "status", void 0);
  __decorate([model_data_1.data], TestModel.prototype, "created_at", void 0);
  __decorate([model_data_1.data], TestModel.prototype, "updated_at", void 0);
  __decorate([model_data_1.data], TestModel.prototype, "someObj", void 0);
  return TestModel;
})(base_model_1.BaseModel);
exports.TestModel = TestModel;
describe("Test S3 Base Model", function() {
  it("Can instantiate properly", function() {
    return __awaiter(void 0, void 0, void 0, function() {
      var instance;
      return __generator(this, function(_a) {
        instance = new TestModel({
          someObj: { id: "test" }
        });
        expect(instance.serialize()).toHaveProperty("id");
        return [2 /*return*/];
      });
    });
  });
  it("Can instantiate properly", function() {
    return __awaiter(void 0, void 0, void 0, function() {
      var instance;
      return __generator(this, function(_a) {
        instance = new TestModel();
        expect(instance.serialize()).toHaveProperty("id");
        return [2 /*return*/];
      });
    });
  });
  it("Collects default values", function() {
    return __awaiter(void 0, void 0, void 0, function() {
      var instance;
      return __generator(this, function(_a) {
        instance = new TestModel({
          id: "some-id",
          name: "Some name",
          description: "Some description"
        });
        expect(instance.defaultValues).toEqual({
          created_at: 123,
          description: "Unknown",
          id: "id-generator",
          name: "Unknown",
          updated_at: undefined,
          someObj: { id: "something" }
        });
        return [2 /*return*/];
      });
    });
  });
  it("Can receive data on construction", function() {
    return __awaiter(void 0, void 0, void 0, function() {
      var instance;
      return __generator(this, function(_a) {
        instance = new TestModel({
          id: "some-id",
          name: "Some name",
          description: "Some description"
        });
        expect(instance.serialize()).toHaveProperty("id");
        expect(instance.serialize()).toHaveProperty("name");
        expect(instance.serialize()).toHaveProperty("description");
        expect(instance.serialize()).toHaveProperty("created_at");
        expect(instance.serialize()).toHaveProperty("updated_at");
        expect(instance.id).toBe("some-id");
        expect(instance.name).toBe("Some name");
        expect(instance.description).toBe("Some description");
        return [2 /*return*/];
      });
    });
  });
  it("Can keep track of changes", function() {
    return __awaiter(void 0, void 0, void 0, function() {
      var instance;
      return __generator(this, function(_a) {
        instance = new TestModel({
          id: "some-id",
          name: "Some name",
          description: "Some description"
        });
        instance.name = "Changed";
        instance.description = "Changed";
        expect(instance.changes).toHaveProperty("name");
        expect(instance.changes).toHaveProperty("description");
        expect(instance.name).toBe("Changed");
        expect(instance.description).toBe("Changed");
        return [2 /*return*/];
      });
    });
  });
});
