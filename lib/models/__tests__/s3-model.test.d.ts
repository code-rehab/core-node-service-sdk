import { S3BaseModel, S3Model } from "../s3-model";
import { RecordData } from "../base-model";
export declare type TestModelRecord = RecordData<{
  id: string;
  name: string;
  description: string;
  status: string;
}>;
declare type Test = S3Model<TestModelRecord & {}>;
export declare class TestModel extends S3BaseModel<TestModelRecord> implements Test {
  readonly S3Bucket: string;
  id: string;
  name: string;
  description: string;
  status: string;
}
export {};
