import { BaseModel, Model, RecordData } from "./base-model";
import * as AWS from "aws-sdk";
import { S3 } from "aws-sdk";
export declare type S3Model<TData extends RecordData = any> = Model<TData> & {
  S3Bucket: string;
  S3Key: string;
  S3Filename: string;
  S3Location(): Promise<string>;
  S3BucketSuffix(): Promise<string>;
  metadata: any;
  S3Meta: S3.HeadObjectOutput;
  fetchMeta(requestParams?: AWS.S3.HeadObjectRequest): Promise<void>;
};
export declare class S3BaseModel<TData extends RecordData> extends BaseModel<TData> implements S3Model {
  protected s3: AWS.S3;
  S3Meta: S3.HeadObjectOutput;
  keyTemplate: string;
  key: string | undefined;
  metadata: any;
  constructor(data: Partial<TData>);
  readonly S3Bucket: string;
  readonly S3Key: string;
  readonly S3Filename: string;
  S3BucketSuffix: () => Promise<string>;
  S3Location: () => Promise<string>;
  fetch: () => Promise<void>;
  save: (requestParams?: S3.PutObjectRequest) => Promise<void>;
  fetchMeta: (requestParams?: S3.HeadObjectRequest) => Promise<void>;
  delete: (requestParams?: S3.DeleteObjectRequest) => Promise<void>;
  private _fetchRecord;
}
