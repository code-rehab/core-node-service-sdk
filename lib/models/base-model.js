"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : ((__.prototype = b.prototype), new __());
    };
  })();
var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __decorate =
  (this && this.__decorate) ||
  function(decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? (desc = Object.getOwnPropertyDescriptor(target, key)) : desc,
      d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
      r = Reflect.decorate(decorators, target, key, desc);
    else
      for (var i = decorators.length - 1; i >= 0; i--)
        if ((d = decorators[i])) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", { value: true });
// import { v4 as uuid } from "uuid";
var model_data_1 = require("../decorators/model-data");
var AbstractBaseModel = /** @class */ (function() {
  function AbstractBaseModel(record) {
    if (record === void 0) {
      record = {};
    }
    this.record = record;
    this.changes = {};
    this.defaultValues = {};
  }
  return AbstractBaseModel;
})();
var BaseModel = /** @class */ (function(_super) {
  __extends(BaseModel, _super);
  function BaseModel() {
    // private _fillable: string[] = [];
    var _this = (_super !== null && _super.apply(this, arguments)) || this;
    _this.serialize = function() {
      return _this.values;
    };
    _this.update = function(data) {
      _this.record = __assign(__assign({}, _this.changes), data);
    };
    _this.take = function(keys) {
      var data = _this.values;
      return keys.reduce(function(result, key) {
        result[key] = data[key];
        return result;
      }, {});
    };
    return _this;
  }
  Object.defineProperty(BaseModel.prototype, "values", {
    get: function() {
      return __assign(__assign(__assign({}, this.defaultValues), this.record), this.changes);
    },
    enumerable: true,
    configurable: true
  });
  __decorate([model_data_1.data], BaseModel.prototype, "id", void 0);
  return BaseModel;
})(AbstractBaseModel);
exports.BaseModel = BaseModel;
