interface DefaultRecordData {
  id: string;
}
interface ModelFunctions<TData extends DefaultRecordData> {
  update(data: Partial<TData>): void;
  serialize(): RecordData<TData>;
  defaultValues: Partial<TData>;
  changes: Partial<TData>;
}
export declare type RecordData<TData extends DefaultRecordData = DefaultRecordData> = TData;
export declare type Model<TData extends RecordData = RecordData> = ModelFunctions<TData> & TData;
declare abstract class AbstractBaseModel<TData extends RecordData> {
  record: Partial<TData>;
  changes: Partial<TData>;
  defaultValues: any;
  constructor(record?: Partial<TData>);
}
export declare class BaseModel<TData extends RecordData> extends AbstractBaseModel<TData> implements Model {
  id: string | undefined;
  serialize: () => TData;
  readonly values: TData;
  update: (data: Partial<TData>) => void;
  take: (keys: (keyof TData)[]) => TData;
}
export {};
