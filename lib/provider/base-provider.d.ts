export declare type WhereOperators = "=" | "==" | "!=" | ">" | "<" | ">=" | "<=";
export interface Provider {
  find(id: string): Promise<any>;
  list(): Promise<any>;
  create(data: any): Promise<any | undefined>;
  update(data: any): Promise<boolean>;
  delete(id: string): Promise<boolean>;
  where(key: keyof any, operator: WhereOperators, value: string | number | boolean): Promise<any>;
  whereIn(key: keyof any, values: Array<string | number | boolean>): Promise<any[]>;
}
declare abstract class AbstractProvider {}
export declare class BaseProvider extends AbstractProvider {}
export {};
