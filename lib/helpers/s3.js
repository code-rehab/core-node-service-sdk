"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.extractKeyData = function(keyMap, key) {
  var values = key.split("/");
  var map = keyMap.split("/");
  return map.reduce(function(d, key, index) {
    if (key.startsWith(":")) {
      d[key.substr(1, key.length)] = values[index];
    }
    return d;
  }, {});
};
exports.formatRecordKey = function(keyMap, data) {
  return keyMap
    .split("/")
    .map(function(key) {
      if (key.startsWith(":")) {
        return data[key.substr(1, key.length)] || "undefined";
      }
      return key;
    })
    .join("/");
};
