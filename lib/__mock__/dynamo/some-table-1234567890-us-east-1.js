"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.record1 = {
  id: "some-id-1",
  name: "Example name",
  description: "Example description"
};
exports.record2 = {
  id: "some-id-2",
  name: "Example name",
  description: "Example description"
};
exports.record3 = {
  id: "some-id-3",
  name: "Example name",
  description: "Example description"
};
exports.sometableRecords = [exports.record1, exports.record2, exports.record3];
