"use strict";
var __importDefault =
  (this && this.__importDefault) ||
  function(mod) {
    return mod && mod.__esModule ? mod : { default: mod };
  };
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_mock_1 = require("aws-sdk-mock");
var fs_1 = require("fs");
var aws_sdk_1 = require("aws-sdk");
var v4_1 = __importDefault(require("uuid/v4"));
var path_1 = __importDefault(require("path"));
var some_table_1234567890_us_east_1_1 = require("./__mock__/dynamo/some-table-1234567890-us-east-1");
process.env.AWS_Region = "us-east-1";
process.env.Account = "1234567890";
jest.mock("aws-xray-sdk-core", function() {
  return {
    captureSDK: function(SDK) {
      return SDK;
    },
    captureAsyncFunc: function(_name, func) {
      return func({ close: function() {} });
    },
    getSegment: function(_name, _rootId, _parentId) {
      return {
        addNewSubsegment: function(_subname) {
          return {
            addAnnotation: function() {},
            addMetadata: function() {},
            close: function() {}
          };
        }
      };
    }
  };
});
aws_sdk_mock_1.mock("S3", "getObject", function(params, callback) {
  callback(null, { Body: Buffer.from(fs_1.readFileSync("src/__mock__/s3/" + params.Bucket + "/" + params.Key)) });
});
aws_sdk_mock_1.mock("S3", "listObjectsV2", function(params, callback) {
  var root = "src/__mock__/s3/" + params.Bucket + "/" + params.Prefix;
  var list = [];
  function traverseDir(dir) {
    fs_1.readdirSync(dir).forEach(function(file) {
      var fullPath = path_1.default.join(dir, file);
      if (fs_1.lstatSync(fullPath).isDirectory()) {
        traverseDir(fullPath);
      } else {
        list.push(fullPath.replace(root, ""));
      }
    });
  }
  traverseDir(root);
  callback(null, {
    Contents: list.map(function(key) {
      return { Key: (params.Prefix || "") + key };
    })
  });
});
aws_sdk_mock_1.mock("S3", "putObject", function(_params, callback) {
  callback(null, {
    ETag: v4_1.default(),
    Location: "PublicWebsiteLink",
    Key: "RandomKey",
    Bucket: "TestBucket",
    VersionId: v4_1.default()
  });
});
aws_sdk_mock_1.mock("S3", "copyObject", function(_params, callback) {
  callback(null, {
    ETag: v4_1.default(),
    Location: "PublicWebsiteLink",
    Key: "RandomKey",
    Bucket: "TestBucket",
    VersionId: v4_1.default()
  });
});
aws_sdk_mock_1.mock("DynamoDB", "query", function(_params, callback) {
  callback(null, {
    Items: some_table_1234567890_us_east_1_1.sometableRecords.map(function(item) {
      return aws_sdk_1.DynamoDB.Converter.marshall(item);
    }),
    Count: some_table_1234567890_us_east_1_1.sometableRecords.length
  });
});
aws_sdk_mock_1.mock("S3", "deleteObject", function(_params, callback) {
  callback(null, {
    ETag: v4_1.default(),
    Location: "PublicWebsiteLink",
    Key: "RandomKey",
    Bucket: "TestBucket",
    VersionId: v4_1.default()
  });
});
aws_sdk_mock_1.mock("S3", "headObject", function(_params, callback) {
  callback(null, {
    ETag: "Some ETag",
    Location: "PublicWebsiteLink",
    Key: "RandomKey",
    Bucket: "TestBucket",
    VersionId: "Some VersionId",
    Metadata: {
      "x-amz-meta-key": "some meta key"
    }
  });
});
