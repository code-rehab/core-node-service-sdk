export interface DataDecorator {
  defaultValues: {};
  changes: {};
  instance?: any;
  initialized: Record<string, boolean>;
}
export declare function data(target: any, key: string): void;
