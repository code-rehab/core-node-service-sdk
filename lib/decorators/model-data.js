"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// export function data(target: any, key: string) {
//   // delete target[key];
//   if (!target["_data_decorator"]) {
//     Object.defineProperty(target, "_data_decorator", {
//       value: {
//         defaultValues: {}
//       }
//     });
//   }
//   const decorator: DataDecorator = target["_data_decorator"];
//   // decorator.defaultValues[key] = decorator.instance[key];
//   target["defaults"] = target[key];
//   Object.defineProperty(target, key, {
//     get(this: typeof arguments[0]) {
//       return (this.changes && this.changes[key]) || (this.record && this.record[key]) || decorator.defaultValues[key];
//     },
//     set(this: typeof arguments[0], val) {
//       this.changes = this.changes || {};
//       this.changes[key] = val;
//     },
//     enumerable: true,
//     configurable: true
//   });
// }
function data(target, key) {
  makePropertyMapper(target, key, function(value) {
    return value;
  });
}
exports.data = data;
function makePropertyMapper(prototype, key, mapper) {
  Object.defineProperty(prototype, key, {
    set: function(firstValue) {
      Object.defineProperty(this, key, {
        get: function() {
          return (this.changes && this.changes[key]) || (this.record && this.record[key]) || this.defaultValues[key];
        },
        set: function(value) {
          this.changes = this.changes || {};
          this.changes[key] = mapper(value);
        },
        enumerable: true
      });
      this["defaultValues"] = this["defaultValues"] || {};
      this["defaultValues"][key] = firstValue;
    },
    enumerable: true,
    configurable: true
  });
}
