import { ListObjectsV2Request } from "aws-sdk/clients/s3";
import { RecordData } from "../models/base-model";
import { S3BaseModel } from "../models/s3-model";
import { Collection } from "./base-collection";
interface S3CollectionConfig {
  keyMap: string;
}
export declare class S3ObjectCollection<
  TModel extends S3BaseModel<TData>,
  TData extends RecordData = {
    id: string;
    updated_at: number;
    created_at: number;
  }
> extends Collection<TModel, TData> {
  private _s3;
  config: S3CollectionConfig;
  readonly S3Bucket: string;
  readonly S3Prefix: string;
  S3BucketSuffix: () => Promise<string>;
  createRecord: (data: Partial<TData>, persist?: boolean) => Promise<TModel>;
  fetch: (optionalParams?: Partial<ListObjectsV2Request>) => Promise<void>;
  private _insert;
}
export {};
