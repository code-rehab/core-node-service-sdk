import { DynamoDB } from "aws-sdk";
import { RecordData } from "../models/base-model";
import { S3BaseModel } from "../models/s3-model";
import { Collection } from "./base-collection";
export declare class DynamoDBCollection<
  TModel extends S3BaseModel<TData>,
  TData extends RecordData = {
    id: string;
    updated_at: number;
    created_at: number;
  }
> extends Collection<TModel, TData> {
  private _dynamo;
  readonly keysToStore: Array<keyof TData>;
  tableName: () => Promise<string>;
  tableSuffix: () => Promise<string>;
  createRecord: (data: Partial<TData>, persist?: boolean) => Promise<TModel>;
  fetch: (params: Partial<DynamoDB.QueryInput>) => Promise<TModel[]>;
  private _insert;
}
