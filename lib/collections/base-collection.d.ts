import { BaseModel, RecordData } from "../models/base-model";
export declare class Collection<TModel extends BaseModel<TData>, TData extends RecordData> {
  records: Record<string, TModel>;
  model: {
    new (data: Partial<TData>): TModel;
  };
  readonly list: TModel[];
  createRecord: (data: Partial<TData>) => Promise<TModel>;
}
