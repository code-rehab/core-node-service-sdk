"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype = b === null ? Object.create(b) : ((__.prototype = b.prototype), new __());
    };
  })();
var __assign =
  (this && this.__assign) ||
  function() {
    __assign =
      Object.assign ||
      function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function(resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function(thisArg, body) {
    var _ = {
        label: 0,
        sent: function() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function() {
          return this;
        }),
      g
    );
    function verb(n) {
      return function(v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (!((t = _.trys), (t = t.length > 0 && t[t.length - 1])) && (op[0] === 6 || op[0] === 2)) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = require("aws-sdk");
var helpers_1 = require("../helpers");
var base_collection_1 = require("./base-collection");
var s3_1 = require("../helpers/s3");
var S3ObjectCollection = /** @class */ (function(_super) {
  __extends(S3ObjectCollection, _super);
  function S3ObjectCollection() {
    var _this = (_super !== null && _super.apply(this, arguments)) || this;
    _this._s3 = new aws_sdk_1.S3();
    _this.config = {
      keyMap: ":id"
    };
    _this.S3BucketSuffix = function() {
      return __awaiter(_this, void 0, void 0, function() {
        var _a, _b;
        return __generator(this, function(_c) {
          switch (_c.label) {
            case 0:
              _a = "-";
              return [4 /*yield*/, helpers_1.getAccountID()];
            case 1:
              _b = _a + _c.sent() + "-";
              return [4 /*yield*/, helpers_1.getRegion()];
            case 2:
              return [2 /*return*/, _b + _c.sent()];
          }
        });
      });
    };
    _this.createRecord = function(data, persist) {
      if (persist === void 0) {
        persist = false;
      }
      return __awaiter(_this, void 0, void 0, function() {
        var record;
        return __generator(this, function(_a) {
          switch (_a.label) {
            case 0:
              record = new this.model(data);
              record.update(data);
              record.keyTemplate = this.S3Prefix + this.config.keyMap;
              if (!persist) return [3 /*break*/, 2];
              return [4 /*yield*/, record.save()];
            case 1:
              _a.sent();
              _a.label = 2;
            case 2:
              return [2 /*return*/, (this.records[record.id] = record)];
          }
        });
      });
    };
    _this.fetch = function(optionalParams) {
      if (optionalParams === void 0) {
        optionalParams = {};
      }
      return __awaiter(_this, void 0, void 0, function() {
        var params, _a, _b, _c, _d, response, err_1;
        return __generator(this, function(_e) {
          switch (_e.label) {
            case 0:
              _e.trys.push([0, 6, , 7]);
              _a = [__assign({}, optionalParams)];
              _b = {};
              _c = optionalParams.Bucket;
              if (_c) return [3 /*break*/, 2];
              _d = this.S3Bucket;
              return [4 /*yield*/, this.S3BucketSuffix()];
            case 1:
              _c = _d + _e.sent();
              _e.label = 2;
            case 2:
              params = __assign.apply(void 0, _a.concat([((_b.Bucket = _c), (_b.Prefix = this.S3Prefix), _b)]));
              return [4 /*yield*/, this._s3.listObjectsV2(params).promise()];
            case 3:
              response = _e.sent();
              if (response.Contents) {
                response.Contents.forEach(this._insert);
              }
              if (!response.IsTruncated) return [3 /*break*/, 5];
              params.ContinuationToken = response.NextContinuationToken;
              return [4 /*yield*/, this.fetch(params)];
            case 4:
              _e.sent();
              _e.label = 5;
            case 5:
              return [3 /*break*/, 7];
            case 6:
              err_1 = _e.sent();
              throw new Error("Unable to fetch collection from S3 \n" + err_1);
            case 7:
              return [2 /*return*/];
          }
        });
      });
    };
    _this._insert = function(meta) {
      if (meta.Key.endsWith(".json")) {
        var map = _this.S3Prefix + _this.config.keyMap;
        var key = meta.Key;
        var data = s3_1.extractKeyData(map, key);
        if (data.id) {
          var record = new _this.model(data);
          record.S3Meta = meta;
          record.key = meta.Key;
          _this.records[record.id] = record;
        }
      }
    };
    return _this;
  }
  Object.defineProperty(S3ObjectCollection.prototype, "S3Bucket", {
    get: function() {
      return "undefined";
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(S3ObjectCollection.prototype, "S3Prefix", {
    get: function() {
      return "records/";
    },
    enumerable: true,
    configurable: true
  });
  return S3ObjectCollection;
})(base_collection_1.Collection);
exports.S3ObjectCollection = S3ObjectCollection;
