export const record1 = {
  id: "some-id-1",
  name: "Example name",
  description: "Example description"
};

export const record2 = {
  id: "some-id-2",
  name: "Example name",
  description: "Example description"
};

export const record3 = {
  id: "some-id-3",
  name: "Example name",
  description: "Example description"
};

export const sometableRecords = [record1, record2, record3];
