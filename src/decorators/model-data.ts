export interface DataDecorator {
  defaultValues: {};
  changes: {};
  instance?: any;
  initialized: Record<string, boolean>;
}

// export function data(target: any, key: string) {
//   // delete target[key];

//   if (!target["_data_decorator"]) {
//     Object.defineProperty(target, "_data_decorator", {
//       value: {
//         defaultValues: {}
//       }
//     });
//   }

//   const decorator: DataDecorator = target["_data_decorator"];
//   // decorator.defaultValues[key] = decorator.instance[key];

//   target["defaults"] = target[key];

//   Object.defineProperty(target, key, {
//     get(this: typeof arguments[0]) {
//       return (this.changes && this.changes[key]) || (this.record && this.record[key]) || decorator.defaultValues[key];
//     },
//     set(this: typeof arguments[0], val) {
//       this.changes = this.changes || {};
//       this.changes[key] = val;
//     },
//     enumerable: true,
//     configurable: true
//   });
// }

export function data(target: any, key: string) {
  makePropertyMapper(target, key, (value: number) => {
    return value;
  });
}

function makePropertyMapper<T>(prototype: any, key: string, mapper: (value: any) => T) {
  Object.defineProperty(prototype, key, {
    set(firstValue: any) {
      Object.defineProperty(this, key, {
        get() {
          return (this.changes && this.changes[key]) || (this.record && this.record[key]) || this.defaultValues[key];
        },
        set(value: any) {
          this.changes = this.changes || {};
          this.changes[key] = mapper(value);
        },
        enumerable: true
      });

      this["defaultValues"] = this["defaultValues"] || {};
      this["defaultValues"][key] = firstValue;
    },
    enumerable: true,
    configurable: true
  });
}
