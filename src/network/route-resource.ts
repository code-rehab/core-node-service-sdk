import { RouteResolverEvent } from "./route-resolver";

export type ResourceResolver<EventType = RouteResolverEvent, ResponseType = any> = (
  event: EventType
) => Promise<ResponseType>;
export class RouteResource {}
