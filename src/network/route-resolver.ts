import { ServiceRouter } from "./router";
import { captureAWS, getSegment } from "aws-xray-sdk-core";
import { UserProfileRecord } from "../models/user-profile";

export interface RouteResolverEvent<Arguments = any, Source = any, identity = any> {
  route?: string;
  field?: any;
  source?: Source;
  profile?: UserProfileRecord;
  arguments: Arguments;
  identity: identity;
}

export class RouteResolver<Routes extends Record<string, (event: RouteResolverEvent) => any>> {
  public xray: boolean = false;
  public router: ServiceRouter<Routes>;

  constructor(public routes: Routes) {
    this.xray = process.env.xray === "enabled" ? true : false;

    if (this.xray) {
      captureAWS(require("aws-sdk"));
    }

    this.router = new ServiceRouter(this.routes);
  }

  public invoke = async (event: RouteResolverEvent | RouteResolverEvent[], _context: any) => {
    let response = null;
    const route = Array.isArray(event) ? event[0] && event[0].route : event.route;
    const operationText = Array.isArray(event) ? "resolveBatch - " : "Resolve - ";

    let segment;
    if (this.xray) {
      const parentSegment = getSegment();
      segment = parentSegment.addNewSubsegment(operationText + route);

      segment.traceId = parentSegment.id;
      segment.parentId = parentSegment.id;
      segment.origin = parentSegment.name;

      // setSegment(segment);
    }

    try {
      if (Array.isArray(event)) {
        response = await this.resolveBatchEvent(event);
      } else {
        response = await this.resolveEvent(event);
      }
    } catch (err) {
      if (this.xray) {
        segment.addError(err);
      }
      throw new Error("Unable to resolve route: " + route + "\n" + err);
    }

    if (this.xray) {
      segment.addAnnotation("route", route);
      segment.addMetadata("meta", "Some Meta");
      // segment.addAttribute("namespace", "aws");
      // segment.addAttribute("routename", "Route Attr:" + route);
      segment.http = {
        request: {
          url: route,
          method: "POST"
        },
        response: {
          status: 200
        }
      };
      segment.close();
    }

    return response;
  };

  public resolveEvent = async (event: RouteResolverEvent) => {
    return await this.router.get(event.route as keyof Routes)(event);
  };

  public resolveBatchEvent = async (events: RouteResolverEvent[]) => {
    return await Promise.all(events.map(event => this.resolveEvent(event)));
  };
}
