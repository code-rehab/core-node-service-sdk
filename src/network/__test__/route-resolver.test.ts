import { RouteResolver } from "../route-resolver";

const routes = {
  "templates.list": async () => {
    return [];
  },
  "layouts.list": async () => {
    return [];
  }
};

let resolver: RouteResolver<any>;

describe("Test route resolver", () => {
  beforeEach(() => {
    resolver = new RouteResolver(routes);
  });

  it("can resolve an invoke operation", async () => {
    const result = await resolver.resolveEvent({ route: "templates.list", arguments: "", identity: {} });

    expect(result).toEqual([]);
  });

  it("can resolve a BatchInvoke operation", async () => {
    const result = await resolver.invoke(
      [
        { route: "templates.list", arguments: "", identity: {} },
        { route: "templates.list", arguments: "", identity: {} }
      ],
      {}
    );

    expect(result).toEqual([[], []]);
  });
});
