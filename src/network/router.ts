export class ServiceRouter<Routes> {
  constructor(public routes: Routes) {
    //
  }

  public get = (name: keyof (Routes) | undefined | null): any => {
    return this.routes[name] || this._routeUnhandledError(name);
  };

  private _routeUnhandledError(name: keyof (Routes)) {
    return async () => {
      throw new Error(`Unhandled route with name "${name.toString()}"`);
    };
  }
}
