export const extractKeyData = (keyMap: string, key: string) => {
  const values = key.split("/");
  const map = keyMap.split("/");

  return map.reduce((d, key, index) => {
    if (key.startsWith(":")) {
      d[key.substr(1, key.length)] = values[index];
    }
    return d;
  }, {});
};

export const formatRecordKey = (keyMap: string, data: any) => {
  return keyMap
    .split("/")
    .map(key => {
      if (key.startsWith(":")) {
        return data[key.substr(1, key.length)] || "undefined";
      }
      return key;
    })
    .join("/");
};
