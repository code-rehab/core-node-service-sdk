import { mock } from "aws-sdk-mock";
import { readFileSync, readdirSync, lstatSync } from "fs";
import { S3, DynamoDB } from "aws-sdk";
import uuid from "uuid/v4";
import path from "path";
import { sometableRecords } from "./__mock__/dynamo/some-table-1234567890-us-east-1";

process.env.AWS_Region = "us-east-1";
process.env.Account = "1234567890";

jest.mock("aws-xray-sdk-core", () => {
  return {
    captureSDK: (SDK: any) => SDK,
    captureAsyncFunc: (_name: string, func) => func({ close: () => {} }),
    getSegment: (_name: string, _rootId?: string, _parentId?: string) => ({
      addNewSubsegment: (_subname: string) => ({
        addAnnotation: () => {},
        addMetadata: () => {},
        close: () => {}
      })
    })
  };
});

mock("S3", "getObject", (params: S3.GetObjectRequest, callback) => {
  callback(null, { Body: Buffer.from(readFileSync(`src/__mock__/s3/${params.Bucket}/${params.Key}`)) });
});

mock("S3", "listObjectsV2", (params: S3.ListObjectsV2Request, callback) => {
  const root = `src/__mock__/s3/${params.Bucket}/${params.Prefix}`;
  const list = [];

  function traverseDir(dir) {
    readdirSync(dir).forEach(file => {
      let fullPath = path.join(dir, file);
      if (lstatSync(fullPath).isDirectory()) {
        traverseDir(fullPath);
      } else {
        list.push(fullPath.replace(root, ""));
      }
    });
  }

  traverseDir(root);
  callback(null, { Contents: list.map(key => ({ Key: (params.Prefix || "") + key })) });
});

mock("S3", "putObject", (_params, callback) => {
  callback(null, {
    ETag: uuid(),
    Location: "PublicWebsiteLink",
    Key: "RandomKey",
    Bucket: "TestBucket",
    VersionId: uuid()
  });
});

mock("S3", "copyObject", (_params, callback) => {
  callback(null, {
    ETag: uuid(),
    Location: "PublicWebsiteLink",
    Key: "RandomKey",
    Bucket: "TestBucket",
    VersionId: uuid()
  });
});

mock("DynamoDB", "query", (_params, callback) => {
  callback(null, {
    Items: sometableRecords.map(item => DynamoDB.Converter.marshall(item)),
    Count: sometableRecords.length
  } as DynamoDB.QueryOutput);
});

mock("S3", "deleteObject", (_params, callback) => {
  callback(null, {
    ETag: uuid(),
    Location: "PublicWebsiteLink",
    Key: "RandomKey",
    Bucket: "TestBucket",
    VersionId: uuid()
  });
});

mock("S3", "headObject", (_params, callback) => {
  callback(null, {
    ETag: "Some ETag",
    Location: "PublicWebsiteLink",
    Key: "RandomKey",
    Bucket: "TestBucket",
    VersionId: "Some VersionId",
    Metadata: {
      "x-amz-meta-key": "some meta key"
    }
  } as S3.HeadObjectOutput);
});
