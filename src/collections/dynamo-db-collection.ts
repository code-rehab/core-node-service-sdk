import { DynamoDB } from "aws-sdk";
import { getAccountID, getRegion } from "../helpers";
import { RecordData } from "../models/base-model";
import { S3BaseModel } from "../models/s3-model";
import { Collection } from "./base-collection";

export class DynamoDBCollection<
  TModel extends S3BaseModel<TData>,
  TData extends RecordData = {
    id: string;
    updated_at: number;
    created_at: number;
  }
> extends Collection<TModel, TData> {
  private _dynamo = new DynamoDB();

  public get keysToStore(): Array<keyof TData> {
    return ["id"];
  }

  public tableName = async (): Promise<string> => {
    return "undefined" + this.tableSuffix;
  };

  public tableSuffix = async (): Promise<string> => {
    return `-${await getAccountID()}-${await getRegion()}`;
  };

  public createRecord = async (data: Partial<TData>, persist: boolean = false) => {
    const record = new this.model(data);
    record.update(data);

    if (persist) {
      await this._insert(record);
      await record.save();
    }

    return (this.records[record.id] = record);
  };

  public fetch = async (params: Partial<DynamoDB.QueryInput>) => {
    const list = await this._dynamo
      .query({
        ...(params || {}),
        TableName: params.TableName || (await this.tableName()) + (await this.tableSuffix())
      })
      .promise();

    return list.Items.map(item => new this.model(DynamoDB.Converter.unmarshall(item) as Partial<TData>));
  };

  private _insert = async (somemodel: TModel) => {
    const data: any = somemodel.take(this.keysToStore);

    await this._dynamo
      .putItem({
        TableName: (await this.tableName()) + (await this.tableSuffix()),
        Item: DynamoDB.Converter.marshall(data)
      })
      .promise();
  };
}
