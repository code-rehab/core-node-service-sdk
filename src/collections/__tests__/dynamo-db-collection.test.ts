import { TestModel, TestModelRecord } from "../../models/__tests__/s3-model.test";
import { DynamoDBCollection } from "../dynamo-db-collection";

process.env.AWS_Region = "us-east-1";
process.env.Account = "1234567890";

class TestCollection extends DynamoDBCollection<TestModel, TestModelRecord> {
  public model: any = TestModel;
  public get S3Bucket() {
    return "testmodel";
  }
}

describe("Test Dynamo DB Collection", () => {
  it("Can instantiate properly", async () => {
    const instance = new TestCollection();
    expect(instance.model).toBe(TestModel);
    expect(instance.records).toEqual({});
    expect(await instance.tableSuffix()).toBe("-1234567890-us-east-1");
  });

  it("Can fetch data", async () => {
    const instance = new TestCollection();
    const result = await instance.fetch({});
    expect(result.map(item => item.id)).toEqual(["some-id-1", "some-id-2", "some-id-3"]);
    expect(result[0]).toBeInstanceOf(TestModel);
  });
});
