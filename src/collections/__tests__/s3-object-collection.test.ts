import { S3ObjectCollection } from "../s3-object-collection";
import { TestModel, TestModelRecord } from "../../models/__tests__/s3-model.test";

process.env.AWS_Region = "us-east-1";
process.env.Account = "1234567890";

class TestCollection extends S3ObjectCollection<TestModel, TestModelRecord> {
  public model: any = TestModel;
  public get S3Bucket() {
    return "testmodel";
  }
}

describe("Test S3 Base Model", () => {
  it("Can instantiate properly", async () => {
    const instance = new TestCollection();
    expect(instance.model).toBe(TestModel);
    expect(instance.records).toEqual({});
    expect(instance.S3Bucket).toBe("testmodel");
  });

  it("Can fetch data", async () => {
    const instance = new TestCollection();
    await instance.fetch();
    expect(instance.list.map(item => item.id)).toEqual(["some-id-1", "some-id-2", "some-id-3"]);
    expect(instance.list[0]).toBeInstanceOf(TestModel);
  });
});
