import { ListObjectsV2Request } from "aws-sdk/clients/s3";
import { S3 } from "aws-sdk";
import { getAccountID, getRegion } from "../helpers";
import { RecordData } from "../models/base-model";
import { S3BaseModel } from "../models/s3-model";
import { Collection } from "./base-collection";
import { extractKeyData } from "../helpers/s3";

interface S3CollectionConfig {
  keyMap: string;
}

export class S3ObjectCollection<
  TModel extends S3BaseModel<TData>,
  TData extends RecordData = {
    id: string;
    updated_at: number;
    created_at: number;
  }
> extends Collection<TModel, TData> {
  private _s3 = new S3();

  public config: S3CollectionConfig = {
    keyMap: ":id"
  };

  public get S3Bucket(): string {
    return "undefined";
  }

  public get S3Prefix(): string {
    return "records/";
  }

  public S3BucketSuffix = async () => {
    return `-${await getAccountID()}-${await getRegion()}`;
  };

  public createRecord = async (data: Partial<TData>, persist: boolean = false) => {
    const record = new this.model(data);
    record.update(data);
    record.keyTemplate = this.S3Prefix + this.config.keyMap;

    if (persist) {
      await record.save();
    }

    return (this.records[record.id] = record);
  };

  public fetch = async (optionalParams: Partial<ListObjectsV2Request> = {}) => {
    try {
      const params: ListObjectsV2Request = {
        ...optionalParams,
        Bucket: optionalParams.Bucket || this.S3Bucket + (await this.S3BucketSuffix()),
        Prefix: this.S3Prefix
      };

      const response = await this._s3.listObjectsV2(params).promise();

      if (response.Contents) {
        response.Contents.forEach(this._insert);
      }

      if (response.IsTruncated) {
        params.ContinuationToken = response.NextContinuationToken;
        await this.fetch(params);
      }
    } catch (err) {
      throw new Error("Unable to fetch collection from S3 \n" + err);
    }
  };

  private _insert = (meta: S3.Object) => {
    if (meta.Key.endsWith(".json")) {
      const map = this.S3Prefix + this.config.keyMap;
      const key = meta.Key;
      const data: Partial<TData> = extractKeyData(map, key);

      if (data.id) {
        const record = new this.model(data);
        record.S3Meta = meta;
        record.key = meta.Key;
        this.records[record.id] = record;
      }
    }
  };
}
