import { BaseModel, RecordData } from "../models/base-model";

export class Collection<TModel extends BaseModel<TData>, TData extends RecordData> {
  public records: Record<string, TModel> = {};
  public model: { new (data: Partial<TData>): TModel };

  public get list() {
    return Object.keys(this.records).map(key => this.records[key]);
  }

  public createRecord = async (data: Partial<TData>) => {
    return new this.model(data);
  };
}
