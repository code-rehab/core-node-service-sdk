// import { v4 as uuid } from "uuid";
import { data } from "../decorators/model-data";

interface DefaultRecordData {
  id: string;
}

interface ModelFunctions<TData extends DefaultRecordData> {
  update(data: Partial<TData>): void;
  serialize(): RecordData<TData>;
  defaultValues: Partial<TData>;
  changes: Partial<TData>;
}

export type RecordData<TData extends DefaultRecordData = DefaultRecordData> = TData;
export type Model<TData extends RecordData = RecordData> = ModelFunctions<TData> & TData;

abstract class AbstractBaseModel<TData extends RecordData> {
  public changes: Partial<TData> = {};
  public defaultValues: any = {};

  constructor(public record: Partial<TData> = {}) {}
}

export class BaseModel<TData extends RecordData> extends AbstractBaseModel<TData> implements Model {
  // private _fillable: string[] = [];

  @data id: string | undefined;

  public serialize = (): TData => {
    return this.values;
  };

  public get values(): TData {
    return { ...this.defaultValues, ...this.record, ...this.changes } as TData;
  }

  public update = (data: Partial<TData>) => {
    this.record = { ...this.changes, ...data };
  };

  public take = (keys: Array<keyof TData>) => {
    const data = this.values;
    return keys.reduce(
      (result, key) => {
        result[key] = data[key];
        return result;
      },
      {} as TData
    );
  };
}
