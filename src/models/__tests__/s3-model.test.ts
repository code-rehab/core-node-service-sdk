import { S3BaseModel, S3Model } from "../s3-model";
import { RecordData } from "../base-model";
import { data } from "../../decorators/model-data";

process.env.AWS_Region = "us-east-1";
process.env.Account = "1234567890";

export type TestModelRecord = RecordData<{ id: string; name: string; description: string; status: string }>;
type Test = S3Model<
  TestModelRecord & {
    // Additional public props
  }
>;

export class TestModel extends S3BaseModel<TestModelRecord> implements Test {
  public get S3Bucket() {
    return "testmodel";
  }

  @data public id: string = "";
  @data public name: string = "";
  @data public description: string = "";
  @data public status: string = "";
}

describe("Test S3 Base Model", () => {
  it("Can instantiate properly", async () => {
    const instance: Test = new TestModel({ id: "some-id-1" });
    const location = await instance.S3Location();

    expect(location).toBe("testmodel-1234567890-us-east-1/some-id-1/data.json");
    expect(instance.serialize()).toHaveProperty("id");
    expect(instance.serialize()).toHaveProperty("name");
    expect(instance.serialize()).toHaveProperty("description");
  });

  it("Can be initiated with predefined data", async () => {
    const instance: Test = new TestModel({ id: "some-id-1", name: "Some name" });
    expect(instance.serialize()).toHaveProperty("id");
    expect(instance.serialize()).toHaveProperty("name");
    expect(instance.serialize()).toHaveProperty("description");
  });
});
