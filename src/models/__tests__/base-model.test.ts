import { BaseModel, RecordData, Model } from "../base-model";
import { data } from "../../decorators/model-data";

type TestModelRecord = RecordData<{ id: string; name: string; description: string; status: string; someObj: any }>;
type Test = Model<
  TestModelRecord & {
    // Additional public props
  }
>;

export class TestModel extends BaseModel<TestModelRecord> implements Test {
  @data public id: string = "id-generator";
  @data public name: string = "Unknown";
  @data public description: string = "Unknown";
  @data public status: string | undefined;
  @data public created_at: number | undefined = 123;
  @data public updated_at: number | undefined = undefined;
  @data public someObj: any = { id: "something" };
}

describe("Test S3 Base Model", () => {
  it("Can instantiate properly", async () => {
    const instance: Test = new TestModel({
      someObj: { id: "test" }
    });

    expect(instance.serialize()).toHaveProperty("id");
  });

  it("Can instantiate properly", async () => {
    const instance: Test = new TestModel();
    expect(instance.serialize()).toHaveProperty("id");
  });

  it("Collects default values", async () => {
    const instance = new TestModel({
      id: "some-id",
      name: "Some name",
      description: "Some description"
    });

    expect(instance.defaultValues).toEqual({
      created_at: 123,
      description: "Unknown",
      id: "id-generator",
      name: "Unknown",
      updated_at: undefined,
      someObj: { id: "something" }
    });
  });

  it("Can receive data on construction", async () => {
    const instance = new TestModel({
      id: "some-id",
      name: "Some name",
      description: "Some description"
    });

    expect(instance.serialize()).toHaveProperty("id");
    expect(instance.serialize()).toHaveProperty("name");
    expect(instance.serialize()).toHaveProperty("description");
    expect(instance.serialize()).toHaveProperty("created_at");
    expect(instance.serialize()).toHaveProperty("updated_at");

    expect(instance.id).toBe("some-id");
    expect(instance.name).toBe("Some name");
    expect(instance.description).toBe("Some description");
  });

  it("Can keep track of changes", async () => {
    const instance = new TestModel({
      id: "some-id",
      name: "Some name",
      description: "Some description"
    });

    instance.name = "Changed";
    instance.description = "Changed";

    expect(instance.changes).toHaveProperty("name");
    expect(instance.changes).toHaveProperty("description");

    expect(instance.name).toBe("Changed");
    expect(instance.description).toBe("Changed");
  });
});
