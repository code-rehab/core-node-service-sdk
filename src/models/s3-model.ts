import { BaseModel, Model, RecordData } from "./base-model";
import * as AWS from "aws-sdk";
import { getAccountID, getRegion } from "../helpers";
import { S3 } from "aws-sdk";
import { formatRecordKey } from "../helpers/s3";

export type S3Model<TData extends RecordData = any> = Model<TData> & {
  S3Bucket: string;
  S3Key: string;
  S3Filename: string;
  S3Location(): Promise<string>;
  S3BucketSuffix(): Promise<string>;
  metadata: any;
  S3Meta: S3.HeadObjectOutput;
  fetchMeta(requestParams?: AWS.S3.HeadObjectRequest): Promise<void>;
};

export class S3BaseModel<TData extends RecordData> extends BaseModel<TData> implements S3Model {
  protected s3 = new S3();
  public S3Meta: S3.HeadObjectOutput = {};
  public keyTemplate: string = ":id";
  public key: string | undefined;
  public metadata: any = {};

  constructor(data: Partial<TData>) {
    super(data);
  }

  public get S3Bucket(): string {
    return "undefined";
  }

  public get S3Key(): string {
    return this.key || formatRecordKey(this.keyTemplate, this) + this.S3Filename;
  }

  public get S3Filename(): string {
    return `/data.json`;
  }

  public S3BucketSuffix = async () => {
    return `-${await getAccountID()}-${await getRegion()}`;
  };

  public S3Location = async () => {
    return `${this.S3Bucket}${await this.S3BucketSuffix()}/${this.S3Key}`;
  };

  public fetch = async () => {
    this.record = await this._fetchRecord();
  };

  public save = async (requestParams?: AWS.S3.PutObjectRequest) => {
    try {
      const dataToStore = this.serialize();
      const params: AWS.S3.PutObjectRequest = {
        Bucket: this.S3Bucket + (await this.S3BucketSuffix()),
        Key: this.S3Key,
        ...(requestParams || {}),
        Metadata: { ...this.metadata }
      };

      const existingData = await this._fetchRecord({ Bucket: params.Bucket, Key: params.Key }).catch(_err => {});

      if (existingData) {
        params.Body = JSON.stringify({ ...existingData, ...dataToStore });
      } else {
        params.Body = JSON.stringify({ ...dataToStore });
      }

      const key = formatRecordKey(this.keyTemplate, this) + this.S3Filename;
      if (this.key !== key) {
        await this.delete();
        this.key = params.Key = key;
      }

      const response = await this.s3.putObject(params).promise();

      this.S3Meta.ETag = response.ETag;
      this.S3Meta.VersionId = response.VersionId;
    } catch (err) {
      throw new Error("Unable to store data on S3 \n" + err);
    }
  };

  public fetchMeta = async (requestParams?: AWS.S3.HeadObjectRequest) => {
    try {
      const params = {
        Bucket: this.S3Bucket + (await this.S3BucketSuffix()),
        Key: this.S3Key,
        ...(requestParams || {})
      };

      const S3Response = await this.s3.headObject(params).promise();
      this.S3Meta.ETag = S3Response.ETag;
      this.S3Meta.VersionId = S3Response.VersionId;
      this.key = params.Key;
    } catch (err) {
      throw new Error("Unable to fetch Metadata from S3 \n" + err);
    }
  };

  public delete = async (requestParams?: S3.DeleteObjectRequest) => {
    try {
      const params = {
        Bucket: this.S3Bucket + (await this.S3BucketSuffix()),
        Key: this.S3Key,
        ...(requestParams || {})
      };

      const response = await this.s3.deleteObject({ Bucket: params.Bucket, Key: params.Key }).promise();
      this.S3Meta.VersionId = response.VersionId;
    } catch (err) {
      throw new Error("Unable to remove object from S3 \n" + err);
    }
  };

  private _fetchRecord = async (requestParams?: AWS.S3.GetObjectRequest) => {
    try {
      const params = {
        Bucket: this.S3Bucket + (await this.S3BucketSuffix()),
        Key: this.S3Key,
        ...(requestParams || {})
      };

      const S3Response = await this.s3.getObject(params).promise();
      const response = JSON.parse(S3Response.Body && S3Response.Body.toString("utf-8")) as TData;
      this.S3Meta.ETag = S3Response.ETag;
      this.S3Meta.VersionId = S3Response.VersionId;
      this.key = params.Key;
      return { ...this.record, ...response };
    } catch (err) {
      throw new Error("Unable to fetch data from S3 \n" + err);
    }
  };
}
