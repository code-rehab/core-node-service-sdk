import { Model, BaseModel, RecordData } from "./base-model";
import uuid from "uuid/v4";
import { data } from "../decorators/model-data";

type Permissions = {
  allow: string[];
  deny: string[];
};

export interface UserProfileRecord extends RecordData {
  id: string;
  role: string;
  profile: string;
  groups: string[];
  permissions: Permissions;
}

export interface UserProfile extends Model<UserProfileRecord> {
  can(permission: string | string[]): boolean;
}

export class UserProfileModel extends BaseModel<UserProfileRecord> implements UserProfile {
  @data public id = uuid();
  @data public role = "";
  @data public profile = "";
  @data public groups = [];
  @data public created_at = 0;
  @data public updated_at = 0;
  @data public permissions: Permissions = {
    allow: [],
    deny: []
  };

  public can = (permission: string | string[]): boolean => {
    let result = { allowed: false, denied: false };

    if (typeof permission === "string") {
      permission = [permission];
    }

    result = permission.reduce((result, p) => {
      return {
        allowed: result.allowed && this._checkPermissions(p, this.permissions.allow),
        denied: result.denied || this._checkPermissions(p, this.permissions.deny)
      };
    }, result);

    return result.allowed === true && result.denied === false;
  };

  private _grants = (requestedPermission: string, profilePermission: string) => {
    const isPermissionGroup: boolean = profilePermission.indexOf(".*") !== -1;
    const groupName = profilePermission.replace(".*", "");

    return {
      // for example: *
      fullAccess: profilePermission === "*",

      // for example: component.* or component.resource.*
      componentAccess: isPermissionGroup && requestedPermission.startsWith(groupName),

      // for example component.resource.permission
      permissionAccess: requestedPermission === profilePermission
    };
  };

  private _checkPermissions = (permission: string, toValidate) => {
    let match = false;

    for (const profilePermission in toValidate) {
      const granted = this._grants(permission, profilePermission);

      if (granted.fullAccess || granted.componentAccess || granted.permissionAccess) {
        match = true;
        break;
      }
    }

    return match === true;
  };
}
