import { STS } from "aws-sdk";

export const getAccountID = async () => {
  if (!process.env.Account) {
    const identity = await new STS().getCallerIdentity().promise();
    process.env.Account = identity.Account;
  }

  return process.env.Account;
};

export const setAccountID = (id: string) => {
  process.env.Account = id;
  return process.env.Account;
};

export const getRegion = async () => {
  return process.env.AWS_Region;
};
