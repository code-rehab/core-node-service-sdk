import { Service } from "../service";
import { RouteResolver } from "../../network/route-resolver";
import context from "aws-lambda-mock-context";

class SomeResource {
  public handler = () => {
    return "Hello World";
  };
}

const service = new Service<any, any, any>(
  ({ resource }) =>
    new RouteResolver({
      "some.route": resource.handler
    }),
  {
    resources: {
      resource: SomeResource
    },
    providers: {},
    services: {}
  }
);

describe("Test Service Initialisation", () => {
  it("Can instantiate properly", async () => {
    expect(service).toBeInstanceOf(Service);
  });

  it("Can invoke properly", async () => {
    expect(await service.invoke({ route: "some.route" }, context())).toBe("Hello World");
  });

  it("Throws errors if route doesn't exist", async () => {
    try {
      await service.invoke({ route: "unknown.route" }, context());
    } catch (err) {
      expect(err).toBeTruthy();
    }

    try {
      await service.invoke({}, context());
    } catch (err) {
      expect(err).toBeTruthy();
    }
  });
});
