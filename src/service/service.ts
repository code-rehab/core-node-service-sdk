type ClassRecord<T> = Record<keyof T, { new (...args: any[]): T[keyof T] }>;

interface ServiceModules<Resources, Providers, Services> {
  resources: ClassRecord<Resources>;
  providers: ClassRecord<Providers>;
  services: ClassRecord<Services>;
}

interface Resolver {
  invoke(event, context): any;
}

interface ServiceOptions {
  xray: boolean;
}

interface ServiceConfig extends ServiceOptions {}

type ServiceResolver<Resources> = (resources: Resources) => Resolver;

export class Service<Resources, Providers, Services> {
  private _modules = {
    provider: {},
    service: {},
    resource: {}
  };

  public resources: Resources = {} as Resources;
  public services: Services = {} as Services;
  public providers: Providers = {} as Providers;
  public resolver: Resolver;
  public config: ServiceConfig = {
    xray: true
  };

  constructor(
    private _serviceResolver: ServiceResolver<Resources>,
    private _moduleClasses: ServiceModules<Resources, Providers, Services>,
    options?: Partial<ServiceOptions>
  ) {
    if (options) {
      this.config = { ...this.config, ...options };
    }

    Object.keys(this._moduleClasses.services).forEach(key => {
      this._registerService(key, this._moduleClasses.services[key]);
    });

    Object.keys(this._moduleClasses.providers).forEach(key => {
      this._registerProvider(key, this._moduleClasses.providers[key]);
    });

    Object.keys(this._moduleClasses.resources).forEach(key => {
      this._registerResource(key, this._moduleClasses.resources[key]);
    });

    this.resolver = this._serviceResolver(this.resources);
  }

  public invoke = async (_event: any, _context: any) => {
    return await this.resolver.invoke(_event, _context);
  };

  /**
   * Registers a Resource class ready to be lazy loaded
   * @param key name of the Service
   * @param ClassName Class of the Service
   */
  private _registerService = (key: string, ClassName: ClassRecord<Resources>[any]) => {
    Object.defineProperty(this.services, key, {
      get: () => {
        if (!this._modules.service[key]) {
          this._modules.service[key] = new ClassName();
        }
        return this._modules.service[key];
      }
    });
  };

  /**
   * Registers a Resource class ready to be lazy loaded
   * @param key name of the Provider
   * @param ClassName Class of the Provider
   */
  private _registerProvider = (key: string, ClassName: ClassRecord<Resources>[any]) => {
    Object.defineProperty(this.providers, key, {
      get: () => {
        if (!this._modules.provider[key]) {
          this._modules.provider[key] = new ClassName();
        }
        return this._modules.provider[key];
      }
    });
  };

  /**
   * Registers a Resource class ready to be lazy loaded
   * @param key name of the resource
   * @param ClassName Class of the Resource
   */
  private _registerResource = (key: string, ClassName: ClassRecord<Resources>[any]) => {
    Object.defineProperty(this.resources, key, {
      get: () => {
        if (!this._modules.resource[key]) {
          this._modules.resource[key] = new ClassName(this);
        }
        return this._modules.resource[key];
      }
    });
  };
}
